#!/bin/bash

BINARY="server"
PROJECT_PATH="golang/src/grafana-json"

init_data (){
    LOCAL=0
    if [ "$1" == "local" ]; then
        LOCAL=1
    fi

    if [ "${LOCAL}" -eq 0 ]; then
        #Remote / gitlab ci
        echo "Building ${BINARY}_linux remotely"
        cd ..
        docker run --rm -e "BINARY=${BINARY}" -e "PROJECT_PATH=${PROJECT_PATH}" -v "$PWD/${PROJECT_PATH}":/go/src/grafana-json -w /go/src/grafana-json golang:1.8 make
        mv "./${PROJECT_PATH}/${BINARY}_linux" ./image && \
        cd image
    else
        #Local build
        cd ..
        echo "Building ${BINARY}_linux locally"
	    docker run --rm -e "BINARY=${BINARY}" -e "PROJECT_PATH=${PROJECT_PATH}" -v "$PWD/${PROJECT_PATH}":/go/src/grafana-json -w /go/src/grafana-json golang:1.8 make
        mv "./${PROJECT_PATH}/${BINARY}_linux" ./image
        cd ./image
    fi
}

cleanup_data () {
    echo "Removing ${BINARY}_linux"
    rm -f "${BINARY}_linux"
}
