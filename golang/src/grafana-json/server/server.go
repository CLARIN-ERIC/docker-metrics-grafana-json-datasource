package server

import (
	"log"
	"fmt"
	"net/http"
	"github.com/rs/cors"
	"github.com/gorilla/mux"
	"io/ioutil"
	"encoding/json"
	"grafana-json/grafana/input"
	"grafana-json/loader/backup"
	"grafana-json/loader/hosts"
)

var context_path string = ""
var hostname string = ""

type fn func () interface{}

type GrafanaJsonBackend struct {
	targets map[string] fn
}

func (b *GrafanaJsonBackend) getTargets() []string {
	result := []string{}
	for key, _ := range b.targets {
		result = append(result, key)
	}
	return result
}

/*
func (b *GrafanaJsonBackend) addTarget(target string, handler target_handler) {
	b.targets[target] = target_handler
}
*/

var backend GrafanaJsonBackend = GrafanaJsonBackend{targets: map[string] fn {}}

func StartServerAndblock(port int, hostname, path, base_path string) {

	fmt.Printf("Loading data from %s\n", base_path)

	h, err := hosts.New(base_path)
	if err != nil {
		log.Fatal(err)
	}

	b, err := backup.NewBackup(base_path)
	if err != nil {
		log.Fatal(err)
	}

	backend.targets["host_info"] = h.GenerateHostsInfoTable
	backend.targets["host_deploy_info"] = h.GenerateHostsDeployInfoTable
	backend.targets["host_project_info"] = h.GenerateHostsProjectInfoTable
	backend.targets["host_timestamps"] = h.GenerateHostsTimestampTable
	backend.targets["backups"] = b.GenerateBackupStatusTable

	for key, f := range backend.targets {
		fmt.Printf("Loading data for: %s\n", key)
		f()
	}
	/*
	host_data := loadHostData()
	for key, _ := range host_data {
		backend.targets[fmt.Sprintf("%s_info", key)] = generateHostsInfoTable
		backend.targets[fmt.Sprintf("%s_deploy_info", key)] = generateHostsDeployInfoTable
	}
	*/


	//Configure CORS
	originsOk := []string{"*"}
	c := cors.New(cors.Options{
		AllowedOrigins: originsOk,
		AllowCredentials: true,
	})

	//Start server
	address := fmt.Sprintf("%s:%d", hostname, port)
	log.Printf("Starting http server on port: %d. Host=%s, Path=%s\n", port, hostname, path)
	log.Fatal(http.ListenAndServe(address, c.Handler(RegisterGrafanaJsonApi())))
}


func RegisterGrafanaJsonApi() *mux.Router {
	router := &mux.Router{}
	//Required
	router.HandleFunc("/", root).Methods("GET", "PUT", "POST", "DELETE")
	router.HandleFunc("/search", search).Methods("GET", "PUT", "POST", "DELETE")
	router.HandleFunc("/query", query).Methods("GET", "PUT", "POST", "DELETE")
	router.HandleFunc("/annotations", requestLogger).Methods("GET", "PUT", "POST", "DELETE")
	//Optional
	//router.HandleFunc("/tag-keys", a.dashboardWithExcludes).Methods("GET")
	//router.HandleFunc("/tag-values", a.dashboardWithExcludes).Methods("GET")
	return router
}

func WriteResponse(w http.ResponseWriter, http_status int,  body interface{}) {
	w.Header().Add("Content-Type", "application/json")
	w.WriteHeader(http_status)
	json.NewEncoder(w).Encode(body)
}

func log_request(r *http.Request) {
	url := r.URL
	//TODO: check headers

	fmt.Printf("[REQUEST] url=%s\n ", url.String())

	fmt.Printf("  Headers:\n")
	for key, values :=  range r.Header {
		formatted_val := ""
		for _, value := range values {
			if formatted_val == "" {
				formatted_val = value
			} else {
				formatted_val = fmt.Sprintf("%s,%s", formatted_val, value)
			}
		}
		fmt.Printf("    %s=%s\n", key, formatted_val)
	}

	s_body := "empty"
	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		fmt.Printf("Failed to parse body: %s\n", err)
	} else {
		s_body = fmt.Sprintf("%s", body)
	}

	fmt.Printf(" Body:\n%s\n ", s_body)
	fmt.Printf(" \n ")
}

func requestLogger(w http.ResponseWriter, r *http.Request) {
	log_request(r)
	w.WriteHeader(http.StatusOK)
}

func root(w http.ResponseWriter, r *http.Request) {
	log_request(r)
	w.WriteHeader(http.StatusOK)
}

func search(w http.ResponseWriter, r *http.Request) {
	fmt.Printf("[Request] %s\n", r.URL.String())
	WriteResponse(w, http.StatusOK, backend.getTargets())
}


func query(w http.ResponseWriter, r *http.Request) {
	fmt.Printf("[Request] %s\n", r.URL.String())

	qry := input.Query{}
	decoder := json.NewDecoder(r.Body)
	if err := decoder.Decode(&qry); err != nil {
		fmt.Printf("Failed to decode request body. Error: %s\n", err)
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	fmt.Printf("Query:\n")// %v\n", qry)
	fmt.Printf("  Max data points: %d\n", qry.MaxDataPoints)
	fmt.Printf("  Range: \n")
	fmt.Printf("    from=%s, to=%s\n", qry.Range.From, qry.Range.To)
	fmt.Printf("  Targets:\n")
	for _, t := range qry.Targets {
		target := ""
		if t.Target != nil {
			target = *t.Target
		}
		fmt.Printf("    refid=%s,type=%s,target=%s\n", t.RefId, t.Type, target)
	}

	data := []interface{}{}
	for _, target := range qry.Targets {
		if target.Target != nil {
			data = append(data, backend.targets[*target.Target]())

			/*
			json_bytes, err := json.MarshalIndent(data, "","  ")
			if err != nil {
				fmt.Printf("Failed to marshal response to json. Error: %s\n", err)
			} else {
				fmt.Printf("Response:\n%s\n", string(json_bytes))
			}
			*/
		} else {
			fmt.Printf("Unkown target\n")
		}
		/*
		if target.Type == "table" {
			data = append(data, *getTableData(*target.Target))
		} else if target.Type == "timeserie" {
			data = append(data, getTimeSerieData())
		} else {
			fmt.Printf("Unsuported target type: %s\n", target.Type)
		}
		*/
	}
	WriteResponse(w, http.StatusOK, data)

}

/*
func getTimeSerieData() *output.TimeSerie {
	return &output.TimeSerie{
		Target: "",
		Datapoints: []interface{}{
			[]interface{}{1.0, 1450754160000},
			[]interface{}{5.0, 1450754220000},
		},
	}
}
*/