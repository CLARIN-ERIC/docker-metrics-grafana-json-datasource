package hosts

import (
	"path/filepath"
	"os"
	"strings"
	"fmt"
	"io/ioutil"
	"encoding/json"
	"log"
	"grafana-json/grafana/output"
)

type Hosts struct {
	path string
}

func New(base_path string) (*Hosts,error) {
	path := fmt.Sprintf("%s/%s", base_path, "versions")
	if info, err := os.Stat(path); os.IsNotExist(err) {
		return nil, fmt.Errorf("Path %s does not exist\n", path)
	} else if !info.IsDir() {
		return nil, fmt.Errorf("%s is not a directory\n", path)
	}
	return &Hosts{path}, nil
}


func (h *Hosts) GenerateHostsTimestampTable() interface{} {
	data := h.loadHostData()

	tbl := output.Table{
		Type: "table",
		Columns: []output.TableColumn{
			output.TableColumn{Text: "Host", Type: "string"},
			output.TableColumn{Text: "Timestamp", Type: "string"},
		},
		Rows: []interface{}{},
	}
	for key, value := range data {
		tbl.AddRow([]interface{}{
			key,
			value.Timestamp,
		})
	}
	return &tbl
}


func (h *Hosts) GenerateHostsInfoTable() interface{} {
	data := h.loadHostData()

	tbl := output.Table{
		Type: "table",
		Columns: []output.TableColumn{
			output.TableColumn{Text: "Host", Type: "string"},
			output.TableColumn{Text: "Installed packages", Type: "number"},
			output.TableColumn{Text: "Updatable packages", Type: "number"},
			output.TableColumn{Text: "CPUs", Type: "number"},
			output.TableColumn{Text: "Memory", Type: "number"},
		},
		Rows: []interface{}{},
	}
	for key, value := range data {

		tbl.AddRow([]interface{}{
			key,
			len(value.Info.Packages),
			len(value.Info.Updates),
			value.Info.Specs.Cpus,
			value.Info.Specs.Mem,
		})
	}
	return &tbl
}

func (h *Hosts) GenerateHostsDeployInfoTable() interface{} {
	data := h.loadHostData()

	tbl := output.Table{
		Type: "table",
		Columns: []output.TableColumn{
			output.TableColumn{Text: "Host", Type: "string"},
			output.TableColumn{Text: "Docker", Type: "string"},
			output.TableColumn{Text: "Docker-compose", Type: "string"},
			output.TableColumn{Text: "Deploy script", Type: "string"},
			output.TableColumn{Text: "Control script", Type: "string"},
		},
		Rows: []interface{}{},
	}
	for key, value := range data {

		docker_version := "unkown"
		compose_version := "unkown"
		deploy_version := "unkown"
		control_version := "unkown"
		for _, pkg := range value.Info.Packages {
			if pkg.Name == "docker-ce.x86_64" {
				docker_version = pkg.Version
			} else if pkg.Name == "docker-compose" {
				compose_version = pkg.Version
			}
		}

		for _, asset := range value.Info.Assets {
			if asset.Name == "deploy-script" {
				deploy_version = asset.Version
			} else if asset.Name == "control-script" {
				control_version = asset.Version
			}
		}

		tbl.AddRow([]interface{}{
			key,
			docker_version,
			compose_version,
			deploy_version,
			control_version})
	}
	return &tbl
}


func (h *Hosts) GenerateHostsProjectInfoTable() interface{} {
	data := h.loadHostData()

	tbl := output.Table{
		Type: "table",
		Columns: []output.TableColumn{
			output.TableColumn{Text: "Host", Type: "string"},
			output.TableColumn{Text: "Project", Type: "string"},
			output.TableColumn{Text: "Version", Type: "string"},
		},
		Rows: []interface{}{},
	}
	for key, value := range data {
		for _, project := range value.Info.Projects {
			tbl.AddRow([]interface{}{
				key,
				project.Name,
				project.Version,
			})
		}
	}
	return &tbl
}




func (h *Hosts) loadHostData() (map[string]*HostInfo) {
	var files []string

	err := filepath.Walk(h.path, func(path string, info os.FileInfo, err error) error {
		if !info.IsDir() && strings.HasSuffix(path, ".json") {
			files = append(files, path)
		}
		return nil
	})
	if err != nil {
		fmt.Printf("Failed to list files in %s. Error: %s\n", h.path, err)
	}

	data := map[string]*HostInfo{}

	for _, file := range files {
		file_bytes, err := ioutil.ReadFile(file)
		if err != nil {
			log.Printf("Failed to read file: %s. Error=%s\n", file, err)
			continue
		}

		hostInfo := HostInfo{}
		if err := json.Unmarshal([]byte(file_bytes), &hostInfo); err != nil {
			log.Printf("Failed to unmarshall %s as json. Error=%s\n", file, err)
		} else {
			//log.Printf("Host=%s, timestamp=%s\n", hostInfo.Hostname, hostInfo.Timestamp)
			data[hostInfo.Hostname] = &hostInfo
		}
	}

	return data
}

type HostInfo struct {
	Timestamp string `json:"timestamp"`
	Hostname string `json:"host"`
	Info HostInfoProjects `json:"projects"`
}

type HostInfoProjects struct {
	Assets []HostItem `json:"assets"`
	Projects []HostItem `json:"projects"`
	Packages []HostItem `json:"packages"`
	Updates []HostItem `json:"updates"`
	//advisories
	Specs HostSpecs `json:"specs"`
}

type HostItem struct {
	Name string `json:"name"`
	Version string `json:"version"`
}

type HostSpecs struct {
	Cpus int64 `json:"cpus"`
	Mem int64 `json:"mem"`

	/*
		"disks": [
	{"disk": "fd0", "size": 4096}
		,{"disk": "sda", "size": 268435456000}
		,{"disk": "sdb", "size": 21474836480}
		,{"disk": "sdc", "size": 268435456000}
	]}*/
}
