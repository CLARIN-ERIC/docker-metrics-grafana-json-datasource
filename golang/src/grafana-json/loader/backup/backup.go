package backup

import (
	"regexp"
	"fmt"
	"time"
	"path/filepath"
	"os"
	"io/ioutil"
	"encoding/json"
	"log"
	"grafana-json/grafana/output"
)

type Backup struct {
	pattern_regex *regexp.Regexp
	pattern_timestamp string
	path string
}
/*
type Host struct {
	Tstart int64
	Tend int64
	Projects map[string]*Project
	Error *string
}

type Project struct {
	Tstart int64
	Tend int64
	State string
	Path string
	Lines []string
}
*/
func NewBackup(base_path string) (*Backup, error) {
	path := fmt.Sprintf("%s/%s", base_path, "runs")
	if info, err := os.Stat(path); os.IsNotExist(err) {
		return nil, fmt.Errorf("Path %s does not exist\n", path)
	} else if !info.IsDir() {
		return nil, fmt.Errorf("%s is not a directory\n", path)
	}

	pattern_timestamp := "2006-01-02_150405"
	pattern_regex, err := regexp.Compile("run_([0-9][0-9][0-9][0-9]-[0-9][0-9]-[0-9][0-9]_[0-9][0-9][0-9][0-9][0-9][0-9]).json")
	if err != nil {
		return nil, err
	}

	return &Backup{pattern_regex, pattern_timestamp, path}, nil
}

func (b *Backup) GenerateBackupStatusTable() interface{} {

	data := b.loadBackupData()

	tbl := output.Table{
		Type: "table",
		Columns: []output.TableColumn{
			output.TableColumn{Text: "Host", Type: "string"},
			output.TableColumn{Text: "Projects", Type: "string"},
			output.TableColumn{Text: "State", Type: "string"},
			output.TableColumn{Text: "Timestamp", Type: "string"},
			output.TableColumn{Text: "Age", Type: "number"},
		},
		Rows: []interface{}{},
	}

	for key, value := range data.Hosts {
		//fmt.Printf("%s=%v\n", key, value)
		for project_key, project_value := range value.Projects {
			t := time.Unix(project_value.TEnd, 0)
			dur := time.Since(t)
			tbl.AddRow([]interface{} {
				key,
				project_key,
				project_value.State,
				t.String(),
				dur.Hours(),
			})
		}

	}

	return &tbl
}

type HostInput struct {
	TStart int64 `json:"t_start"`
	TEnd int64 `json:"t_end"`
	Projects map[string]*ProjectInput `json:"projects"`
	Error *string `json:"error"`

}

type ProjectInput struct {
	TStart int64 `json:"t_start"`
	TEnd int64 `json:"t_end"`
	State string `json:"state"`
	Path string `json:"path"`
	Lines []string `json:"lines"`
}

type Backups struct {
	Hosts map[string]*HostInput `json:"hosts"`
}

func (b *Backup) listBackups() (int, []string, error) {
	var files []string
	var idx int = -1
	var max_timestamp *time.Time = nil

	var count int = 0
	err := filepath.Walk(b.path, func(path string, info os.FileInfo, err error) error {
		if !info.IsDir() {
			m := b.pattern_regex.FindStringSubmatch(info.Name())
			if m != nil {
				t, err := time.Parse(b.pattern_timestamp, m[1])
				if err != nil {
					fmt.Println("Failed to parse time from %s. Error: %s\n", m[1], err)
				} else {
					if max_timestamp == nil || t.After(*max_timestamp) {
						idx = count
						max_timestamp = &t
					}
				}
			}

			files = append(files, path)
			count++
		}
		return nil
	})
	if err != nil {
		return idx, nil, err
	}
	return idx, files, nil
}

func (b *Backup) loadBackupData() *Backups {

	most_recent_file_idx, paths, err := b.listBackups()
	if err != nil {
		fmt.Printf("Failed to list files in %s. Error: %s\n", b.path, err)
		return &Backups{}
	} /*else {
		fmt.Printf("Listing %s:\n", b.path)
		for idx, p := range paths {
			marker := ""
			if idx >= 0 && idx == most_recent_file_idx {
				marker = " ***"
			}
			fmt.Printf("  file: %s%s\n", p, marker)
		}
	}*/

	file := paths[most_recent_file_idx]
	log.Printf("Loading data from %s\n", file)

	file_bytes, err := ioutil.ReadFile(file)
	if err != nil {
		log.Printf("Failed to read file: %s. Error=%s\n", file, err)
		return nil
	}

	data := Backups{}
	if err := json.Unmarshal([]byte(file_bytes), &data); err != nil {
		log.Printf("Failed to unmarshall %s as json. Error=%s\n", file, err)
		return nil
	}

	return &data
}
