package output

type Table struct {
	Type string `json:"type"`
	Columns []TableColumn `json:"columns"`
	Rows []interface{} `json:"rows"`
}

func (t *Table) AddRow(row []interface{}) {
	t.Rows = append(t.Rows, row)
}

type TableColumn struct {
	Text string `json:"text"`
	Type string `json:"type"`
}

type TimeSerie struct {
	Target string `json:"target"`
	Datapoints []interface{} `json:"datapoints"`
}
