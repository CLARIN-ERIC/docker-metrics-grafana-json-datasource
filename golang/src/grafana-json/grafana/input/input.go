package input


/*
{
  "requestId": "Q101",
  "timezone": "",
  "panelId": 2,
  "dashboardId": null,
  "range": {
    "from": "2019-08-30T01:13:24.654Z",
    "to": "2019-08-30T07:13:24.655Z",
    "raw": {
      "from": "now-6h",
      "to": "now"
    }
  },
  "interval": "30s",
  "intervalMs": 30000,
  "targets": [
    {
      "refId": "A",
      "type": "timeserie"
    }
  ],
  "maxDataPoints": 720,
  "scopedVars": {
    "__interval": {
      "text": "30s",
      "value": "30s"
    },
    "__interval_ms": {
      "text": "30000",
      "value": 30000
    }
  },
  "startTime": 1567149204655,
  "rangeRaw": {
    "from": "now-6h",
    "to": "now"
  },
  "adhocFilters": []
}
 */

type Query struct {
	//"scopedVars": {
	//	"__interval": {
	//	"text": "30s",
	//	"value": "30s"
	//},
	//	"__interval_ms": {
	//	"text": "30000",
	//	"value": 30000
	//}
	//},
	//"adhocFilters": []
	RequestId string `json:"requestId"`
	TimeZone string `json:"timezone"`
	PanelId int64 `json:"panelId"`
	DashboardId *int64 `json:"dashboardId"`
	Interval string `json:"interval"`
	IntervalMs int64 `json:"intervalMs"`
	MaxDataPoints int64 `json:"maxDataPoints"`
	StartTime int64 `json:"startTime"`
	Range *Range `json:"range"`
	RangeRaw *RangeRaw `json:"rangeRaw"`
	Targets []*QueryTarget `json:"targets"`
}

type Range struct {
	From string `json:"from"`
	To string `json:"to"`
	Raw *RangeRaw `json:"raw"`
}

type RangeRaw struct {
	From string `json:"from"`
	To string `json:"to"`
}

type QueryTarget struct {
	RefId string `json:"refId"`
	Type string `json:"type"`
	Target *string `json:"target"`
}
