package main

import (
	"github.com/spf13/cobra"
	"fmt"
	"grafana-json/server"
)

var ServerCmd = &cobra.Command{
	Use:   "server",
	Short: "Shibboleth test server",
	Long: `Control interface for the shibboleth test server.`,
}

func main() {
	var verbose bool

	ServerCmd.PersistentFlags().BoolVarP(&verbose, "verbose", "v", false, "Run in verbose mode")

	ServerCmd.AddCommand(versionCmd)
	ServerCmd.AddCommand(InitStartCommand(&verbose))
	ServerCmd.Execute()
}

func InitStartCommand(verbose *bool) (*cobra.Command) {
	var port int
	var path string
	var hostname string
	var base_path string

	var cmd = &cobra.Command{
		Use:   "start",
		Short: "start server",
		Long: `Start server`,
		Run: func(cmd *cobra.Command, args []string) {
			server.StartServerAndblock(port, hostname, path, base_path)
		},
	}

	cmd.Flags().IntVarP(&port, "port", "p", 8080, "Base port to run the server on")
	cmd.Flags().StringVarP(&path, "path", "P", "/", "Context path to deploy application on")
	cmd.Flags().StringVarP(&hostname, "hostname", "H", "localhost", "Hostname to deploy application on")
	cmd.Flags().StringVarP(&base_path, "base_path", "b", "/data", "Base directory to load data from")

	return cmd;
}

var versionCmd = &cobra.Command{
	Use:   "version",
	Short: "Print the version number of docker-clarin",
	Long:  `All software has versions. This is docker-clarin's.`,
	RunE: func(cmd *cobra.Command, args []string) error {
		printCheckerVersion()
		return nil
	},
}

func printCheckerVersion() {
	fmt.Printf("Shibboleth test server v%s by CLARIN ERIC\n", "1.0.0-beta")
}
